<?php
namespace common\assets\plugins;

use yii\web\AssetBundle;

class InputMaskAssets extends AssetBundle
{
    public $sourcePath = '@common/assets/node_modules/inputmask/';

    public $css = [
    ];
    public $js = [
        'dist/min/jquery.inputmask.bundle.min.js',
        'dist/inputmask/inputmask.numeric.extensions.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}