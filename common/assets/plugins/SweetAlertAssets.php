<?php
namespace common\assets\plugins;

use yii\web\AssetBundle;

class SweetAlertAssets extends AssetBundle
{
    public $sourcePath = '@common/assets/node_modules/sweetalert/';

    public $css = [
    ];
    public $js = [
        'dist/sweetalert.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}