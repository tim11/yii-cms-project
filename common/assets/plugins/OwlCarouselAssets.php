<?php
namespace common\assets\plugins;

use yii\web\AssetBundle;

class OwlCarouselAssets extends AssetBundle
{
    public $sourcePath = '@common/assets/node_modules/owl.carousel/';

    public $css = [
        'dist/assets/owl.carousel.min.css',
        'dist/assets/owl.theme.default.min.css',
    ];
    public $js = [
        'dist/owl.carousel.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}