<?php
    Yii::setAlias("@common", COMMON_DIR);
    Yii::setAlias("@frontend", FRONTEND_DIR);
    Yii::setAlias("@backend", BACKEND_DIR);
    Yii::setAlias("@console", CONSOLE_DIR);
    Yii::setAlias("@runtime", COMMON_DIR . "/runtime");
    Yii::setAlias("@vendor", APP_DIR . "/vendor");