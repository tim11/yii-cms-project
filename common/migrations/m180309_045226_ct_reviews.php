<?php

use yii\db\Schema;
use yii\db\Migration;

class m180309_045226_ct_reviews extends Migration
{

    private $tableName = "{{%reviews}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
        }
        try {
            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(11)->notNull()->comment('ID'),
                'author' => $this->string(255)->notNull()->comment('Автор'),
                'text' => $this->text()->notNull()->comment('Текст отзыва'),
                'state' => $this->smallInteger()->defaultValue(STATE_INACTIVE)->comment('Состояние'),
                'created_at' => $this->integer()->comment('Добавлен'),
                'updated_at' => $this->integer()->comment('Изменен')
            ], $tableOptions);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
