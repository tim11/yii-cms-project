<?php

use yii\db\Schema;
use yii\db\Migration;

class m180309_052502_ut_reviews extends Migration
{

private $tableName = "{{%reviews}}";
public function up()
{
    try{
        $this->addColumn($this->tableName, "image", Schema::TYPE_STRING."(255)");
        $this->addCommentOnColumn($this->tableName, "image", "Фотография");
    //insert code
    }catch(Exception $e){
    echo $e->getMessage();
    }
    }

public function down()
{
    try{
    $this->dropColumn($this->tableName, "image");
    }catch(Exception $e){
    echo $e->getMessage();
    }
    return true;
}
}
