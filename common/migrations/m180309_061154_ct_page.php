<?php

use yii\db\Schema;
use yii\db\Migration;

class m180309_061154_ct_page extends Migration
{

    private $tableName = "{{%page}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
        }
        try {
            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(11)->notNull()->comment('ID'),
                'link' => $this->string(20)->unique()->comment('Линк'),
                'title' => $this->string(255)->notNull()->comment('Заголовок'),
                'text' => $this->text()->notNull()->comment('Текст страницы'),
                'created_at' => $this->integer()->comment('Добавлен'),
                'updated_at' => $this->integer()->comment('Изменен')
            ], $tableOptions);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
