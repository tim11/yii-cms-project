<?php

use yii\db\Schema;
use yii\db\Migration;

class m180309_053935_ct_shop extends Migration
{

    private $tableName = "{{%shop}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
        }
        try {
            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(11)->notNull()->comment('ID'),
                'category' => $this->integer()->defaultValue(0)->comment('Категория'),
                'name' => $this->string(255)->notNull()->comment('Название'),
                'description' => $this->text()->comment('Описание'),
                'price' => $this->decimal(10,2)->defaultValue(0)->comment('Базовая цена'),
                'image' => $this->string(255)->comment('Фотография'),
                'created_at' => $this->integer()->comment('Добавлен'),
                'updated_at' => $this->integer()->comment('Изменен')
            ], $tableOptions);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
