<?php

namespace common\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%reviews}}".
 *
 * @property int $id ID
 * @property string $author Автор
 * @property string $text Текст отзыва
 * @property string $image Фотография
 * @property int $state Состояние
 * @property int $created_at Добавлен
 * @property int $updated_at Изменен
 */
class Reviews extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reviews}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author', 'text'], 'required'],
            [['text'], 'string'],
            [['state', 'created_at', 'updated_at'], 'integer'],
            [['author', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'author' => Yii::t('app', 'Автор'),
            'text' => Yii::t('app', 'Текст отзыва'),
            'state' => Yii::t('app', 'Состояние'),
            'image' => Yii::t('app', 'Фотография'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
        ];
    }
}
