<?php
/**
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\widgets\ListView;
use yii\widgets\LinkPager;
$this->title = "Каталог";

?>
<h1><?= $this->title; ?></h1>
<?php
if ($dataProvider->totalCount){
    ?>
    <div id="section-catalog">
        <div class="container">
            <div class="title">Выберите свой крупномер</div>
            <?php
            echo ListView::widget([
                "dataProvider" => $dataProvider,
                "options" => [
                    "tag" => "div",
                    "class" => "items"
                ],
                "itemOptions" => [
                    "tag" => "div",
                    "class" => "block-shop-item"
                ],
                "layout" => "{items}",
                "itemView" => "_shop-item"
            ]);
            if ($dataProvider->pagination->pageCount > 1){
                echo LinkPager::widget([
                    "pagination" => $dataProvider->pagination
                ]);
            }
            ?>
        </div>
    </div>
    <?php
}


?>
