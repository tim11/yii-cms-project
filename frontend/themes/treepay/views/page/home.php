<?php
    use common\models\db\Reviews;
    use common\models\db\Shop;
    use yii\db\Expression;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

/*
 * @var $this yii\web\View
 */
    $this->title = "Home page";
    $this->registerMetaTag([
        "name" => "description",
        "value" => "this is metadescription"
    ]);
    $this->registerMetaTag([
        "name" => "keywords",
        "value" => "this is metadescription"
    ]);

    $this->registerJsFile("https://api-maps.yandex.ru/2.1/?lang=ru_RU");
?>
<div id="section-home-slider">
    <div class="slider owl-carousel owl-theme">
        <div class="item" style="background-image: url('/upload/slider/1.jpg');">
            <div class="container">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Esse, temporibus. Repudiandae consequatur id beatae perferendis dolore unde excepturi dicta voluptatibus saepe doloremque? Beatae doloribus ducimus libero? Odit unde incidunt quisquam.</div>
        </div>
        <div class="item" style="background-image: url('/upload/slider/1.jpg');">
            <div class="container">Ab tempora consectetur, reiciendis labore omnis accusantium animi possimus dolore voluptas nemo sit, quis laboriosam alias rerum facere officia sed. Aut laboriosam ad sit ea numquam sint dignissimos esse vitae.</div>
        </div>
        <div class="item" style="background-image: url('/upload/slider/1.jpg');">
            <div class="container">Minus corrupti ea ratione. Adipisci nesciunt quae suscipit culpa praesentium aspernatur quidem voluptatem recusandae qui perspiciatis. Enim perferendis dolorum ad aperiam. Ratione labore fugiat repellat veniam cum quaerat ipsa voluptatibus!</div>
        </div>
    </div>
</div>
<div id="section-home-trigger">
    <div class="container">
        <div class="row">
            <div class="item">
                <div class="icon"><i class="fa fa-thumbs-up"></i></div>
                <div class="content">100%<br/>приживаемость<br/>деревьев</div>
            </div>
            <div class="item">
                <div class="icon"><i class="fa fa-trophy"></i></div>
                <div class="content">Гарантия на выполненные работы</div>
            </div>
            <div class="item">
                <div class="icon"><i class="fa fa-truck"></i></div>
                <div class="content">Быстрая доставка в Ваш город</div>
            </div>
        </div>
    </div>
</div>
<div id="section-home-best">
    <div class="container">
        <div class="title">Почему выбирают именно нас :</div>
        <div class="elements">
            <div class="item">
                <div class="image"><img src="http://pitomnik-rb.ru/images/price_krupnomeri.gif" alt=""></div>
                <div class="text">Используем только отборные деревья
                </div>
            </div>
            <div class="item">
                <div class="image"><img src="http://pitomnik-rb.ru/images/price_hedge.gif" alt=""></div>
                <div class="text">Выращиваем в питомниках Башкортостана
                </div>
            </div>
            <div class="item">
                <div class="image"><img src="http://pitomnik-rb.ru/images/vehicles_icons_names.png" alt=""></div>
                <div class="text">Используется новейшая спец.техника
                </div>
            </div>
            <div class="item">
                <div class="image"><img src="http://pitomnik-rb.ru/images/pay.png" alt=""></div>
                <div class="text">Оплата после получения
                </div>
            </div>
            <div class="item">
                <div class="image"><img src="http://pitomnik-rb.ru/images/Message-512.png" alt=""></div>
                <div class="text">Проводим бесплатные консультации</div>
            </div>
        </div>
    </div>
</div>
<?php
    $dataProvider = new ActiveDataProvider([
        "query" => Shop::find(),
        "pagination" => [
            "pageSizeLimit" => 12
        ]
    ]);
    if ($dataProvider->totalCount){
        ?>
        <div id="section-catalog">
            <div class="container">
                <div class="title">Выберите свой крупномер</div>
                <?php
                    echo ListView::widget([
                        "dataProvider" => $dataProvider,
                        "options" => [
                            "tag" => "div",
                            "class" => "items"
                        ],
                        "itemOptions" => [
                            "tag" => "div",
                            "class" => "block-shop-item"
                        ],
                        "layout" => "{items}",
                        "itemView" => "_home-shop-item"
                    ]);
                    if ($dataProvider->totalCount > 12){
                        echo Html::a("Показать весь каталог", ["shop/index"], [
                            "class" => "more"
                        ]);
                    }
                ?>
            </div>
        </div>
        <?php
    }


?>



<div id="section-about">
    <div class="container">
        <div class="title">О нас</div>
        <div class="text">
            <div class="block-static-text">
                <p>Компания Крупномеры Башкортостана осуществляет полный комплекс работ по посадке и пересаживанию крупномерных деревьев на территории России с 2009 года.</p>
                <p>
                    Наши деревья выращиваются исключительно в питомниках, им не нужна адаптация, т.к. они выращены в той же климатической среде. Благодаря собственной спец.техники для выкопки, пересадке и доставке растений, мы обеспечиваем сохранность и приживаемость всех
                    посадочных материалов. Мы следим за приживаемостью своих деревьев, даем бесплатные консультации по уходу и проводим сезонное обслуживание.</p>
            </div>
        </div>
    </div>
</div>
<div id="section-feedback">
    <div class="container">
        <div class="title">Остались вопросы? Вы поможем Вам ответить на них!</div>
        <a href="#modal-feedback" class="button" data-fancybox data-src="#modal-feedback">Заказать обратный звонок</a>
    </div>
</div>
<?php
    $reviews = Reviews::find()
        ->where([
            "state" => STATE_ACTIVE
        ])
        ->orderBy(new Expression('rand()'))
        ->limit(5)
        ->all();
    if (!empty($reviews)){
        ?>
<div id="section-reviews">
    <div class="container">
        <div class="title">Что о нас говорят</div>
        <div class="slider owl-carousel owl-theme">
            <?php
                foreach ($reviews as $review){
                    ?>
                    <div class="item">
                        <div class="image">
                            <?php
                                $thumb = !empty($review->image) ? $review->image : "https://t3.ftcdn.net/jpg/01/11/51/56/500_F_111515658_ROB41LcX1Kzg03vOmF6AqHy97JJURc6i.jpg";
                            ?>
                            <img src="<?= $thumb; ?>" alt="<?= Html::encode($review->author); ?>">
                        </div>
                        <div class="name">
                            <?= Html::encode($review->author); ?>
                        </div>
                        <div class="text">
                            <?= Html::encode($review->text); ?>
                        </div>
                    </div>
                    <?php
                }
            ?>
        </div>
    </div>
</div>
        <?php
    }
?>
<div id="section-map">
    <div id="map">

    </div>
</div>


