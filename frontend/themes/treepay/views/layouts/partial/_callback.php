<div class="block-window-dialog" id="modal-feedback">
    <div class="content">
        <div class="title">Заявка на обратный звонок</div>
        <div class="body">
            <form action="#" onsubmit="sendCallback(this); return false;">
                <div class="form-group">
                    <input type="tel" name="phone" placeholder="Укажите Ваш телефон" />
                </div>
                <div class="form-group">
                    <input type="text" name="name" placeholder="Укажите Ваше имя" />
                </div>
                <button class="button" type="submit">Отправить заявку</button>
            </form>

        </div>
    </div>
</div>