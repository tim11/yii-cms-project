<?php
    use yii\widgets\Menu;
    echo Menu::widget([
        "activateItems" => true,
        "activateParents" => true,
        "items" => [
            [
                "label" => "Главная",
                "url" => ["page/home"]
            ],
            [
                "label" => "Каталог",
                "url" => ["shop/index"]
            ],
            [
                "label" => "Наши работы",
                "url" => ["gallery/index"]
            ],
            [
                "label" => "Как мы работаем",
                "url" => ["page/index", "link" => "how-work"]
            ],
            [
                "label" => "Полезная информация",
                "url" => ["page/index", "link" => "info"]
            ],
            [
                "label" => "Контакты",
                "url" => ["page/contact"]
            ],
        ]
    ])
?>