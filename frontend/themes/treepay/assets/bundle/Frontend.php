<?php
namespace frontend\themes\treepay\assets\bundle;

use common\assets\plugins\FontAwesomeAssets;
use yii\web\AssetBundle;

class Frontend extends AssetBundle
{
    public $sourcePath = "@frontend/themes/treepay/assets/publish";

    public $css = [
        "css/style.css"
    ];

    public function init()
    {
        $this->js[] = "js/index.js?time=" . time();
        parent::init();
    }

    public $js = [

    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'common\assets\plugins\FancyboxAssets',
        'common\assets\plugins\FontAwesomeAssets',
        'common\assets\plugins\InputMaskAssets',
        'common\assets\plugins\OwlCarouselAssets',
        'common\assets\plugins\SweetAlertAssets',
    ];

    public $publishOptions = [
        'forceCopy'=>true,
    ];

}