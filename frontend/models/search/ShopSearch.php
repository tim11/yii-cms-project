<?php
namespace frontend\models\search;

use common\models\db\Shop;
use yii\data\ActiveDataProvider;

class ShopSearch extends Shop
{
    public $limit = 24;

    public function search($data = []){
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            "query" => $query,
            "pagination" => [
                "defaultPageSize" => $this->limit
            ]
        ]);
        $this->load($data);
        if ($this->validate()){
            //
        }
        return $dataProvider;
    }
}