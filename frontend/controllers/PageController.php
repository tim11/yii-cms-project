<?php
namespace frontend\controllers;

use common\models\db\Page;
use yii\web\NotFoundHttpException;

class PageController extends \yii\web\Controller
{

    public function actionIndex($link){
        $model = Page::find()
            ->where([
                "link" => $link
            ])
            ->one();
        if (empty($model)){
            throw new NotFoundHttpException("Страница не найдена");
        }
        return $this->render("index", [
            "model" => $model
        ]);
    }
    public function actionContact(){
        echo "contact";
    }
    public function actionHome(){
        return $this->render("home", [
            "time" => time()
        ]);
    }
}