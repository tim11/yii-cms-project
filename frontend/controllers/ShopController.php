<?php
/**
 * Created by PhpStorm.
 * User: timj
 * Date: 09.03.2018
 * Time: 10:58
 */

namespace frontend\controllers;


use frontend\models\search\ShopSearch;
use yii\web\Controller;
use Yii;

class ShopController extends Controller
{
    public function actionIndex(){
        $searchModel = new ShopSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render("index", [
            "searchModel" => $searchModel,
            "dataProvider" => $dataProvider
        ]);
    }
}