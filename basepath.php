<?php
	defined("APP_DIR") or define("APP_DIR", __DIR__);
	defined("COMMON_DIR") or define("COMMON_DIR", APP_DIR . "/common");
	defined("FRONTEND_DIR") or define("FRONTEND_DIR", APP_DIR . "/frontend");
	defined("BACKEND_DIR") or define("BACKEND_DIR", APP_DIR . "/backend");
	defined("CONSOLE_DIR") or define("CONSOLE_DIR", APP_DIR . "/console");
	