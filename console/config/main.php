<?php
use yii\helpers\ArrayHelper;

$console_config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'migrate' => [
            'migrationPath' => '@common/migrations',
            'class' => 'yii\console\controllers\MigrateController',
            'templateFile' => '@console/components/views/Migration.php'
        ],
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $console_config['bootstrap'][] = 'gii';
    $console_config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return ArrayHelper::merge(
    is_file(COMMON_DIR . "/config/main.php") ? require COMMON_DIR . "/config/main.php" : [],
    $console_config,
    is_file(CONSOLE_DIR . "/config/main.local.php") ? require CONSOLE_DIR . "/config/main.local.php" : []
);
